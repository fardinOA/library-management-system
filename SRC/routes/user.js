const {
    
    userResisterController, 
    userLoginControllerl,
    allUser,
    allStudent,
    allTeacher,
    allLibrarian,
    uniqueUser,
    userUpdateController,
    userDelete,
    userDeletePermanent,
    changePassword,
    allUserPageWise,
    forgotPassword,
    resetPassword


}=require('../controller/user');

const auth=require('../../middelware/auth')
const permission=require('../../middelware/permission');



const express=require('express');
const router=express.Router();

router.post('/register',userResisterController);
router.post('/login',userLoginControllerl);
router.get('/allStudent',auth, permission(['librarian','student','teacher']),allStudent)
router.get('/allTeacher',auth, permission(['librarian','teacher']),allTeacher);
router.get('/allLibrarian',auth, permission(['librarian']),allLibrarian);
router.get('/uniqueUser/:userName', auth, permission(['librarian', 'student', 'teacher']),uniqueUser);
router.put('/update/:id',auth,userUpdateController);
router.put('/delete/:id',auth, permission(['librarian']),userDelete);
router.put('/change-password/:id',auth,changePassword)
router.delete('/delete-permanent/:id',auth,permission(['librarian']),userDeletePermanent);
router.get('/allUser', auth, permission(['teacher','lybrarian']), allUser);
router.get('/allUser-page-wise/:page',auth, permission(['librarian', 'student', 'teacher']),allUserPageWise);
router.post('/forgot-password',forgotPassword);
router.post('/reset-password',resetPassword);


module.exports=router;

