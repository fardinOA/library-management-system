const jwt=require('jsonwebtoken');
const secreat = process.env.JWT_SECRETE;



module.exports=function (req,res,next) {
    const token = req.header('Authorization');
    if(!token){
        return res.status(401).json({
            message:'Unauthorized user'
        })
    }
    
    const verified=jwt.verify(token,secreat);
    req.user=verified;
    next();
}