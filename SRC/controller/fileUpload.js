const singleFileUploadController=async(req,res)=>{
    console.log(req.file);
    res.send('file Upload successfull');
}

const multiFileUploadController=async(req,res)=>{
    console.log(req.files);
    res.send('multi file Upload successfull');
}

module.exports={
    singleFileUploadController,
    multiFileUploadController
}