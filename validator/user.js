const Joi = require('joi');

const addressSchema=Joi.object({
    division:Joi.string(),
    district: Joi.string(),
    upazila: Joi.string(),
    zipCode: Joi.string(),
    area: Joi.string(),
})

const userValidator = Joi.object({
    userName: Joi.string().required()
        .alphanum().min(4).max(15),
    userType: Joi.string(),
    address:addressSchema,
    email: Joi.string().trim().required()
        .regex(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/),
    password: Joi.string().required()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
})

const changePassValidator=Joi.object({
    currentPassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
    newPassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
    confirmNewPassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
})
const option={
    abrotEarly:false,
    allowUnknown:true,
    srtipUnknown:true
}

module.exports={
    userValidator,
    changePassValidator,
    option
}
