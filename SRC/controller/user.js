const User=require('../modeles/user');
const bcrypt=require('bcrypt');
const Joi=require('joi')
const jwt=require('jsonwebtoken')
const {userValidator,changePassValidator,option}=require('../../validator/user');
const nodemailer=require('nodemailer');


const userResisterController=async(req,res)=>{
      try{
                const {error,value}=userValidator.validate(req.body);
                if(error){
                    res.status(400).json({
                        message:'Invalid information',
                        Error:error.details[0].message
                    })
                }
                else{
                    const { userName, userType,address, email, password } = req.body
                   // const hashPassword = await bcrypt.hash(password, 10);
                    let user = new User({
                        userName,
                        userType,
                        address,
                        email,
                        password
                    })
                    let isSave = await user.save();

                    if (isSave) {
                        res.status(201).json({
                            message: 'user create successfully',
                            User: isSave
                        })
                    } else {
                        res.json({
                            msg: 'can not create user'
                        })
                    }

                }

              
      }
      catch(err){
            res.json({err})
      }
}

const userLoginControllerl=async(req,res)=>{
    try{
       const { email, password } = req.body;

       const userInfo = await User.findOne({ email });
       

       if (userInfo&&userInfo.isDeleted==false) {
           let comp = await bcrypt.compare(password, userInfo.password);

           let data={
               userName:userInfo.userName,
               userType:userInfo.userType
           }
           const secrateKey = process.env.JWT_SECRETE
           const token = jwt.sign(data, secrateKey, { expiresIn: '1h' })

           if (comp) {
               res.json({
                   message: 'Welcome to you account',
                   Avater:userInfo.profilePic,
                    token
               })
           } else {
               res.json({
                   message: 'password does not match'
               })
           }
       } else {
           res.json({
               message: 'User does not found'
           })
       }
    }catch(err){
       res.json({err})
    }
   
}

const allUser=async(req,res)=>{
    try{
            let all=await User.find({isDeleted:false});
           
            if(all){
                res.json({
                    message:`Total ${all.length} users`,
                    allUser:all
                })
            }
            else{
                res.json({
                    message: `Total ${all.length} users`,
                    message:'Do not have any user yet!'
                })
            }
    }
    catch(err){
        res.json({err})
    }
}

const allUserPageWise=async (req,res)=>{
    try{
        const page=req.params.page;
     
        var skipPage=0;
        var userLimit=5;
         if(page>1){
             skipPage=(page-1)*(userLimit);
         }
        const user= await User.find().skip(skipPage).limit(userLimit);
        if(user){
            res.status(200).json({
                "Page":page,
                "Total User":user.length,
                user
            })
        }else{
            res.status(200).json({
                msg:"user not found"
            })
        }
        

    }
    catch(err){
        res.json({
            err
        })
    }
}

const allStudent=async(req,res)=>{
    try{
        let uType="student"
        let student = await User.find({ userType: uType, isDeleted: false});
        if(student){
            res.json({
                message:`Total ${student.length} student`,
                students:student
            })
        }
        else{
            res.json({
                message:'no student resistered yet'
            })
        }
    }
    catch(err){
        res.json({
            message: 'no student resistered yet',
            err
        })
    }
}

const allTeacher = async (req, res) => {
    try {
        let sType = "teacher"
        let teacher = await User.find({ userType: sType, isDeleted: false });
        if (teacher) {
            res.json({
                message: `Total ${teacher.length} teacher`,
                teachers:teacher
            })
        }
        else {
            res.json({
                message: 'no teacher resistered yet'
            })
        }
    }
    catch (err) {
        res.json({
             message: 'no teacher resistered yet',
             err 
       })
    }
}


const allLibrarian = async (req, res) => {
    try {
        let lType = "librarian"
        let librarian = await User.find({ userType: lType, isDeleted: false });
        if (teacher) {
            res.json({
                message: `Total ${teacher.length} librarian`,
                librarians: librarian
            })
        }
        else {
            res.json({
                message: 'no librarian resistered yet'
            })
        }
    }
    catch (err) {
        res.json({
            message: 'no librarian resistered yet',
            err
        })
    }
}

const uniqueUser=async (req,res)=>{
    try {
        const { userName } = req.params;
        let userInfo = await User.findOne({userName});

        if (userInfo) {
            res.json({
                user: userInfo
            })
        }
        else {
            res.json({
                message: "user not found"
            })
        }
    }
    catch (error) {
        
        res.json({
            message: "user not found",
            error
        })
    }
}

const userUpdateController=async(req,res)=>{
    try{
        const _id = req.params.id;
        await User.findByIdAndUpdate(
            { _id },
            {
                $set: req.body
            },
            {
                multi: true
            }
        )
        return res.json({
            message:'User updated successfully',
            updatedUser:req.body
        })
    }
    catch(err){
        res.json({err})
    }
}

const changePassword=async(req,res)=>{
    try{

        //password validation
        const { error, value } = changePassValidator.validate(req.body);
        if (error) {
            res.status(400).json({
                message: 'Invalid information',
                Error: error.details[0].message
            })
        }

        //updating new password
        const _id=req.params.id;
        const { currentPassword, newPassword, confirmNewPassword}=req.body;
        const user=await User.findOne({_id,isDeleted:false});
        if(user){
            const comp=await bcrypt.compare(currentPassword,user.password);
            if(comp){
                
                // check the password is same to courrent password or not
                const comp2=await bcrypt.compare(newPassword,user.password);
                if(comp2){
                    return res.json({
                        message:'You are changing your password to current password'
                    })
                }
               
                
                if(newPassword===confirmNewPassword){
                    const hashPassword=await bcrypt.hash(newPassword,10);
                    await User.findByIdAndUpdate(
                        { _id },
                        {
                            $set: {password:hashPassword}
                        },
                        {
                            multi: true
                        }
                    )
                    return res.json({
                        msg:'password changed',

                    })
                }
                else{
                    res.json({
                        msg:'password does not match'
                    })
                }

            }else{
                res.json({
                    msg:'Invalid password'
                })
            }

        }else{
            res.json({
                msg:'user not found'
            })
        }

    }
    catch(err){
        res.json({err})
    }
}

const userDelete=async(req,res)=>{
    try{
        const _id=req.params.id;
        await User.findByIdAndUpdate(
            {_id},
            {
                $set: { isDeleted:true}
            }
        )
        return res.json({
            message:'user temporary deleted'
        })
    }
    catch(err){
        res.json({err})
    }
}

const userDeletePermanent=async(req,res)=>{
    try{
        const _id=req.params.id;
        await User.findOneAndDelete({_id});
        return res.json({
            message:'user deleted successfull'
        })
    }
    catch(err){
        res.json({err})
    }
}

const forgotPassword=async (req,res)=>{
    try{
        const {email}=req.body;
        const user=await User.findOne({email});
        if(!user){
            return res.status(400).json({
                msg:'user with this mail does not exit'
            })
        }
        const secrateKey = process.env.JWT_SECRETE
        const token = jwt.sign({ _id: user._id }, secrateKey,{expiresIn:'5m'});
       await user.updateOne({resetLink:token});

    //    node mailaer //
    // First Step //
        const Email = process.env.NODEMAILER_EMAIL
        const pass = process.env.NODEMAILER_EMAIL_PASS
         let transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: Email,
                pass: pass
            }
        })
        

        // const transporter = nodemailer.createTransport({
        //     host: 'smtp.ethereal.email',
        //     port: 587,
        //     auth: {
        //         user: 'madelynn.pfeffer@ethereal.email',
        //         pass: 'YtK6mN67v3UvQDFnNx'
        //     }
        // });
        

        // second step //

        let mailOption={
            from:'mdrabby7410@gmail.com',
            to:user.email,
            subject:'Reset Password Link',
            text:token
        }

        // third step //
        transporter.sendMail(mailOption,function (err,data) {
            if(err){
                return res.json({
                    msg:'Can not send email',
                    err
                })
            }
            else{
                console.log(data);
                return res.json({
                    msg:'Email send successfully'
                })
            }
        })
    }
    catch(err){
        res.json({err})
    }
}


const resetPassword=async (req,res)=>{
    try{
        const secrateKey = process.env.JWT_SECRETE
        const {token,newPassword}=req.body;
        var hashPassword=await bcrypt.hash(newPassword,10);
        const isTrue=jwt.verify(token,secrateKey);

        if(isTrue){
            const user=await User.findOne({resetLink:token});
            if(user){
               await user.updateOne({
                    password:hashPassword,
                    resetLink:''
                })
                return res.json({
                    msg: 'Password Reset Successfully'
                })
            }
            else{
                res.json({
                    msg:'Invalid token or expired'
                })
            }
            
        }
        else{
            res.json({
                msg:'user not found fot this token'
            })
        }

    }catch(err){
        res.json({err})
    }
}

module.exports={
    userResisterController,
    userLoginControllerl,
    allUser,
    allStudent,
    allTeacher,
    allLibrarian,
    uniqueUser,
    userUpdateController,
    userDelete,
    userDeletePermanent,
    changePassword,
    allUserPageWise,
    forgotPassword,
    resetPassword
}