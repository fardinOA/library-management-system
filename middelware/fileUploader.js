const multer=require('multer');

const fileStorageEngine=multer.diskStorage({
    destination:(req,file,cb)=>{
        cb(null,'./image')
    },
    filename:(req,file,cb)=>{
        cb(null,Date.now()+'-'+file.originalname)
    }
})

const maxFileSize=10000000;
const upload=multer({
    storage:fileStorageEngine,
    limits:{fileSize:maxFileSize}
})

module.exports=upload;