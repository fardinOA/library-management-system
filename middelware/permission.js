
const jwt_decode=require('jwt-decode');

module.exports=function permission(arr) {
    return (req,res,next)=>{
        try{
            const token = req.header('Authorization');
            const decode = jwt_decode(token);

            const isFind = arr.find(e => {
                return e == decode.userType;
            })
            if (isFind) {
                next();
            } else {
                res.status(401).json({
                    message: 'Resticted Area'
                })
            }
        }
        catch(err){
            res.status(401).json({
                message:'You dont have permission for this api',
                err
            })
        }
    }
    
}



