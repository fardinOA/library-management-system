const express = require('express');
require('dotenv').config();
const mongoose = require('mongoose');
const app = express();


app.use(express.json())

//all routes
const userRouter=require('./SRC/routes/user')
const fileUploaderRouter=require('./SRC/routes/fileUploader')


//mongo connection
const url = process.env.MONGO_URL
mongoose.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true

})
    .then(console.log("server connected"))
    .catch(err => {
        console.log(err);
    })

app.use('/user', userRouter);
app.use('/upload',fileUploaderRouter)


const port = process.env.PORT || 4000
app.get('/', (req, res) => {
    res.send(`<h1>I am from ROOT</h1>`)
})
app.listen(port, console.log("Running on Port ", port));