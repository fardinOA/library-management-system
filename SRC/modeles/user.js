const mongoose=require('mongoose');
const bcrypt=require('bcrypt')
const Schema=mongoose.Schema;

const userSchema=new Schema({
    userName:String,
    userType:{
        type:String,
        enum:["student","teacher","librarian"],
        default:"student"
    },
    address:{
        division:String,
        district:String,
        upazila:String,
        zipCode:String,
        area:String
    },
    email:{
        type:String,
        unique:true
    },
    password:String,
    resetLink:{
        type:String,
        default:''
    },
    isDeleted: {
        type: Boolean,
        default: false
    }
    
   
})

userSchema.pre('save',function(next){
    var user=this;
    if(user.isModified('password')||user.isNew){
        bcrypt.genSalt(10,function (err,saltValue) {
            if(err){
                return next(err)
            }
            bcrypt.hash(user.password,saltValue,function (err,hash) {
                if(err){
                    return next(err);
                }
                if(hash){
                    user.password=hash;
                }
                next();
            })
        })
    }else{
        next();
    }

})

module.exports=mongoose.model('User',userSchema);