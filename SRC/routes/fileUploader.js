const {
    singleFileUploadController,
    multiFileUploadController
}=require('../controller/fileUpload')

const fileUploader=require('../../middelware/fileUploader')
const express=require('express');
const router=express.Router();

router.post('/single', fileUploader.single('image'), singleFileUploadController)
router.post('/multi',fileUploader.array('images',12),multiFileUploadController);

module.exports=router;